Qu'est-ce ?
==========

Écrit en PHP dans un code très basique. Ne servira très probablement qu'à moi ;-)

Permet de mettre à disposition un formulaire pour envoyer un SMS à *son* téléphone à l'aide de l'API fournie par Free Mobile. L'option doit être activée dans le compte pour connaître les informations à mettre dans la config ci-dessous.

Auteur
======
* Nicolas Grandjean nicolas * microniko * net

Installation
============
* Récupérer les sources.
* Copier config.inc.dist.php en config.inc.php
* Éditer config.inc.php en suivant les instructions du fichier.

Utilisation
===========
Soumettre le formulaire avec le code indiqué dans la configuration (permet d'éviter les SPAMS).
