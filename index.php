<!DOCTYPE html>
<html>
<head>
<title><?php echo _("M'envoyer un SMS"); ?></title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<link href="styles.css" type="text/css" rel="stylesheet">
</head>
<body>

<?php


if (file_exists('config.inc.php')) {
include_once('config.inc.php');
}
else {
   echo '<p id="resultat" class="ko">'._('Configuration (<code>config.inc.php</code>) non trouvée').'</p>';
   exit;
   }
if (empty($CODE)){
   echo '<p id="resultat" class="ko">'._('Configuration dans <code>config.inc.php</code> incorrecte : $CODE ne peut pas être vide !').'</p>';
   exit;
}   

// Reporte toutes les erreurs PHP ////////////////////////////////
// Commenter les lignes suivantes pour un système en production !
error_reporting(E_ALL);
ini_set('html_errors', true);
ini_set('display_errors', true);

function secure($toto) {
	//return trim(addslashes($toto));
	return trim(rawurlencode($toto));
	//return trim(htmlentities($toto,ENT_QUOTES, 'UTF-8'));
}

// On convertit le code de la config
$CODE=secure($CODE);

if (isset($_POST)) {
   foreach ($_POST as $clef => $valeur) {
    if (!empty($valeur)) { $P[$clef]=secure($valeur);  }
    }
    }
    
    
	
if (isset($P) and $P['code']==$CODE) {

   //$url=$API.urlencode($P['message']);
   $url=$API.$P['message'];
   
   //echo "Le formulaire a été soumis !";
   $ch = curl_init();
   curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
  curl_setopt( $ch, CURLOPT_URL, $url );
  if (curl_exec($ch)) {
   echo '<p id="resultat" class="ok">'._('MESSAGE ENVOYÉ !').'</p>';
   }
   else {
   	echo '<p id="resultat" class="ko">'._('Quelque chose s\'est mal passé !').'</p>';
   }
   /*
   ?>
   Adresse : <a href="<?php echo $url; ?>"><?php echo $url; ?></a>
   <?php 
   */
// Fin if POST
}
else {
echo $MSG;

?>

<form action="/" method="post">

   <fieldset><legend><label for="message"><?php echo _('Message'); ?></label></legend><textarea name="message" id="message" rows="5" cols="15" required></textarea></fieldset>	
	<fieldset><legend><label for="code"><?php echo _('Code'); ?></label></legend><input type="text" name="code" id="code" required></fieldset>
	<input type="submit" name="envoyer" value="<?php echo _('Envoyer'); ?>">

</form>

<?php 
// Fin else POST
}


?>
<p><?php echo _('Ceci utilise l\'API d\'envoi de SMS fournie par Free Mobile. Écrit en PHP. Sous licence GPL3. <a href="https://gitlab.com/microniko/mysms">Sources</a>.'); ?></p>
</body>
</html>
